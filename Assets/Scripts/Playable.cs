﻿using UnityEngine;

public abstract class Playable : MonoBehaviour
{
    private int movements;

    /// <summary>
    /// It gives the number of moves the player did
    /// </summary>
    public int Movements { get => movements; }
    public void IncrementMovement() => ++movements;
    public abstract void MoveUp();
    public abstract void MoveRight();
    public abstract void MoveDown();
    public abstract void MoveLeft();
}