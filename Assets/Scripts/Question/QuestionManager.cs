﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Scorm;
using System.Linq;
using UnityEngine.SceneManagement;

public class QuestionManager : MonoBehaviour
{
    private const int NUMBER_OF_QUESTION = 4;

    public Text QuestionTitle;
    public List<Toggle> myToggles;
    public Button ValidationButton;
    public Questionnaire MonQuestionnaire;
    public int QuestionCourante = 0;
    public static int bonusScore = 1;
    public static float successPercentage;
    int score = 0;

    private void Start()
    {
        MontrerQuestion();
        ValidationButton.onClick.AddListener(Next);
    }

    private void Next()
    {
        VerifReponse();
        MontrerQuestion();
    }

    private void VerifReponse()
    {
        foreach (var toggle in myToggles)
        {
            if (toggle.isOn)
            {
                var temp = MonQuestionnaire.MaListeDeQuestions[QuestionCourante - 1].MyReponses.FirstOrDefault(rep => rep.BonneReponse);
                if (temp == null) { return; }
                if (toggle.GetComponentInChildren<Text>().text == temp.TexteReponse)
                {
                    score++;
                }
                break;
            }
        }
    }

    private void MontrerQuestion()
    {
        if (QuestionCourante + 1 > MonQuestionnaire.MaListeDeQuestions.Count)
        {
            ValidationButton.onClick.RemoveListener(Next);
            Fin();
            return;
        }
        var question = MonQuestionnaire.MaListeDeQuestions[QuestionCourante];

        QuestionTitle.text = question.Intitule;
        int index = 0;
        foreach (var toggle in myToggles)
        {
            toggle.isOn = false;
            toggle.GetComponentInChildren<Text>().text = question.MyReponses[index].TexteReponse;
            index++;
        }

        QuestionCourante++;
    }

    private void Fin()
    {
        bonusScore = score;
        successPercentage = (bonusScore / NUMBER_OF_QUESTION) * 100;
        SceneManager.LoadScene("Main");
    }
}