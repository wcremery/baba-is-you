﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "question", menuName = "Créér une question", order = 0)]
public class Question : ScriptableObject
{
    public string Intitule;
    public List<Reponse> MyReponses;
}