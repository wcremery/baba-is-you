﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Scorm;

public class GameManager : MonoBehaviour
{
    #region constants
    public const float WIDTH_RANGE = 10 * SpriteLoader.CELL_SIZE;
    public const float HEIGHT_RANGE = 7 * SpriteLoader.CELL_SIZE;
    #endregion
    

    #region fields
    private IScormService scorm;
    private InputHandler playerInputHandler;
    private Command command;
    private InputHistory lastInput;    
    private BabaController baba;
    private Playable currentActor;
    private UIManager userInterface;
    private readonly Stack<InputHistory> commandHistory = new Stack<InputHistory>();
    private bool isScromed = false;
    public static bool isGameOver = false;
    public static int finalScore;
    #endregion

    private void Awake()
    {
        playerInputHandler = GetComponent<InputHandler>();
        InitializeScorm();
    }

    private void InitializeScorm()
    {
#if UNITY_EDITOR
        scorm = new ScormPlayerPrefsService();
#else
                            scorm = new ScormService();
#endif
        bool result = scorm.Initialize(Scorm.Version.Scorm_2004);
        if (result)
        {
            Debug.Log("Communication initialized");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        userInterface = transform.Find("UI").GetComponent<UIManager>();
        baba = transform.Find("PlayArea/Baba").GetComponent<BabaController>();
        currentActor = baba;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isGameOver)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Save();
                SceneManager.LoadScene("Main");
            }
            if (WordController.babaIsYou) ProcessingInputs();
            RefreshUI();
        }
        else
        {            
            if ((!isScromed))
            {                
                Save();
                float score = 10000.0f - DataTransfer.timeElapsed * DataTransfer.moves;
                finalScore = Mathf.FloorToInt(score * QuestionManager.bonusScore);
                scorm.SetMinScore(0);               
                scorm.SetRawScore(finalScore);
                scorm.SetSuspendData("END");
                scorm.Commit();
                isScromed = !isScromed;
            }
            userInterface.GameOver(finalScore);
            if (Input.anyKeyDown) Application.Quit();
        }
    }

    private void Save()
    {
        DataTransfer.timeElapsed += Time.timeSinceLevelLoad;
        DataTransfer.moves += currentActor.Movements;
    }

    #region UI
    private void RefreshUI()
    {
        userInterface.DisplayTime(Time.timeSinceLevelLoad + DataTransfer.timeElapsed);
        userInterface.PlayerMoves.text = "Moves : " + (currentActor.Movements + DataTransfer.moves);
    }    
    #endregion

    #region inputs processing
    private void ProcessingInputs()
    {
        command = playerInputHandler.HandleInput();

        if(command != null)
        {
            ExecuteCommand();
            commandHistory.Push(new InputHistory(command, currentActor));         
        }
        UndoCommand();
    }

    private void ExecuteCommand()
    {
        command.Execute(currentActor);
        currentActor.IncrementMovement();
    }
    private void UndoCommand()
    {
        if (commandHistory.Count > 0 && Input.GetKeyDown(KeyCode.Backspace))
        {
            lastInput = commandHistory.Pop();
            lastInput.LastCommand.Undo(lastInput.LastPlayable);
            currentActor.IncrementMovement();
        }
    }
    #endregion
}
