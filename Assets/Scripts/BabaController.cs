﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabaController : Playable
{
    #region constants    
    private const int NUMBER_ANIMATION = 5;
    private const float ANIMATION_SPEED = .2f;
    #endregion

    #region fields
    private SpriteRenderer babaSprite;
    private Rigidbody2D babaBody;
    private int lastIndex = 0;
    private int startIndex;
    private int downIndex = 0;
    private int topIndex = 0;
    private int leftIndex = 0;
    private int rightIndex = 0;

    public static Vector2 lastDirection;
    #endregion

    // Awake is called before Start
    private void Awake()
    {
        // Initialize its own components
        babaSprite = gameObject.GetComponent<SpriteRenderer>();
        babaBody = gameObject.GetComponent<Rigidbody2D>();        
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(IdleAnimation());
    }

    #region idle method
    // Play the idles animations for the lastIndex
    private IEnumerator IdleAnimation()
    {
        int lastIndexAnimation = lastIndex + SpriteLoader.TOTAL_SPRITE_COLUMN * SpriteLoader.TOTAL_SPRITE_LINE;
        int index = lastIndex;

        while (index <= lastIndexAnimation)
        {
            babaSprite.sprite = SpriteLoader.SpriteSheet[index];
            index += SpriteLoader.TOTAL_SPRITE_COLUMN;
            yield return new WaitForSeconds(ANIMATION_SPEED);
            yield return 0;
        }

        StartCoroutine(IdleAnimation());
    }
    #endregion

    #region movement methods   
    public override void MoveUp()
    {
        startIndex = 5;
        topIndex = (topIndex + 1) % NUMBER_ANIMATION;
        rightIndex = 0;
        downIndex = 0;
        leftIndex = 0;
        lastIndex = startIndex + topIndex;
        babaSprite.sprite = SpriteLoader.SpriteSheet[lastIndex];
        lastDirection = Vector2.up;

        StopAllCoroutines();
        StartCoroutine(IdleAnimation());

        RaycastHit2D hit = Physics2D.Raycast(babaBody.position, Vector2.up, SpriteLoader.CELL_SIZE);
        if (hit.collider != null)
        {
            if (hit.collider.CompareTag("Rock"))
            {
                if (WordController.rockIsWin) GameManager.isGameOver = true;
                Transform rock = transform.parent.Find("RockItem");
                if (rock.transform.position.y < GameManager.HEIGHT_RANGE - 0.24f)
                {
                    if(rock.GetComponent<RockController>().MoveRock())
                        babaBody.MovePosition(babaBody.position + Vector2.up * SpriteLoader.CELL_SIZE);
                }
                    
            }

            if (hit.collider.CompareTag("Word"))
            {
                GameObject word = hit.collider.gameObject;
                if (word.GetComponent<ItemController>().ItemIsMovable(Vector2.up)
                    && word.transform.position.y < GameManager.HEIGHT_RANGE - 0.24f)
                {
                    word.GetComponent<Rigidbody2D>().
                        MovePosition(hit.collider.gameObject.GetComponent<Rigidbody2D>().position + Vector2.up * SpriteLoader.CELL_SIZE);
                    babaBody.MovePosition(babaBody.position + Vector2.up * SpriteLoader.CELL_SIZE);
                }
            }

            if (hit.collider.CompareTag("Flag") && WordController.flagIsWin)
            {
                GameManager.isGameOver = true;
            }
        }
        else if (hit.collider == null  && transform.position.y < GameManager.HEIGHT_RANGE)
        {
            babaBody.MovePosition(babaBody.position + Vector2.up * SpriteLoader.CELL_SIZE);
        }
    }

    public override void MoveDown()
    {
        startIndex = 15;
        downIndex = (downIndex + 1) % NUMBER_ANIMATION;
        topIndex = 0;
        rightIndex = 0;
        leftIndex = 0;
        lastIndex = startIndex + downIndex;
        babaSprite.sprite = SpriteLoader.SpriteSheet[lastIndex];
        lastDirection = Vector2.down;

        StopAllCoroutines();
        StartCoroutine(IdleAnimation());

        RaycastHit2D hit = Physics2D.Raycast(babaBody.position, Vector2.down, SpriteLoader.CELL_SIZE);
        if (hit.collider != null)
        {
            if (hit.collider.CompareTag("Rock"))
            {
                if (WordController.rockIsWin) GameManager.isGameOver = true;
                Transform rock = transform.parent.Find("RockItem");
                if (rock.transform.position.y > -GameManager.HEIGHT_RANGE + 0.24f)
                {
                    if(rock.GetComponent<RockController>().MoveRock())
                        babaBody.MovePosition(babaBody.position + Vector2.down * SpriteLoader.CELL_SIZE);
                }
            }

            if (hit.collider.CompareTag("Word"))
            {
                GameObject word = hit.collider.gameObject;
                if (word.GetComponent<ItemController>().ItemIsMovable(Vector2.down)
                    && word.transform.position.y > -GameManager.HEIGHT_RANGE + 0.24f)
                {
                    word.GetComponent<Rigidbody2D>().
                        MovePosition(hit.collider.gameObject.GetComponent<Rigidbody2D>().position + Vector2.down * SpriteLoader.CELL_SIZE);
                    babaBody.MovePosition(babaBody.position + Vector2.down * SpriteLoader.CELL_SIZE);
                }
            }

            if (hit.collider.CompareTag("Flag") && WordController.flagIsWin)
            {
                GameManager.isGameOver = true;
            }
        }
        else if (hit.collider == null && transform.position.y > -GameManager.HEIGHT_RANGE)
        {
            babaBody.MovePosition(babaBody.position + Vector2.down * SpriteLoader.CELL_SIZE);
        }
    }

    public override void MoveRight()
    {
        startIndex = 0;
        rightIndex = (rightIndex + 1) % NUMBER_ANIMATION;
        topIndex = 0;
        downIndex = 0;
        leftIndex = 0;
        lastIndex = startIndex + rightIndex;
        babaSprite.sprite = SpriteLoader.SpriteSheet[lastIndex];
        lastDirection = Vector2.right;

        StopAllCoroutines();
        StartCoroutine(IdleAnimation());

        RaycastHit2D hit = Physics2D.Raycast(babaBody.position, Vector2.right, SpriteLoader.CELL_SIZE);
        if (hit.collider != null)
        {
            if (hit.collider.CompareTag("Rock"))
            {
                if (WordController.rockIsWin) GameManager.isGameOver = true;
                Transform rock = transform.parent.Find("RockItem");
                if (rock.transform.position.x < GameManager.WIDTH_RANGE - 0.24f)
                {
                    if(rock.GetComponent<RockController>().MoveRock())
                        babaBody.MovePosition(babaBody.position + Vector2.right * SpriteLoader.CELL_SIZE);
                }
            }

            if (hit.collider.CompareTag("Word"))
            {
                GameObject word = hit.collider.gameObject;
                if (word.GetComponent<ItemController>().ItemIsMovable(Vector2.right)
                    && word.transform.position.x < GameManager.WIDTH_RANGE-0.24f)
                {
                    word.GetComponent<Rigidbody2D>().
                        MovePosition(hit.collider.gameObject.GetComponent<Rigidbody2D>().position + Vector2.right * SpriteLoader.CELL_SIZE);
                    babaBody.MovePosition(babaBody.position + Vector2.right * SpriteLoader.CELL_SIZE);
                }
            }

            if (hit.collider.CompareTag("Flag") && WordController.flagIsWin)
            {
                GameManager.isGameOver = true;
            }
        }
        else if (hit.collider == null && transform.position.x < GameManager.WIDTH_RANGE)
        {
            babaBody.MovePosition(babaBody.position + Vector2.right * SpriteLoader.CELL_SIZE);
        }
    }

    public override void MoveLeft()
    {
        startIndex = 10;
        leftIndex = (leftIndex + 1) % NUMBER_ANIMATION;
        topIndex = 0;
        rightIndex = 0;
        downIndex = 0;
        lastIndex = startIndex + leftIndex;
        babaSprite.sprite = SpriteLoader.SpriteSheet[lastIndex];
        lastDirection = Vector2.left;

        StopAllCoroutines();
        StartCoroutine(IdleAnimation());


        RaycastHit2D hit = Physics2D.Raycast(babaBody.position, Vector2.left, SpriteLoader.CELL_SIZE);
        if (hit.collider != null)
        {
            if (hit.collider.CompareTag("Rock"))
            {
                if (WordController.rockIsWin) GameManager.isGameOver = true;
                Transform rock = transform.parent.Find("RockItem");
                if (rock.transform.position.x > -GameManager.WIDTH_RANGE + 0.24f)
                {
                    if(rock.GetComponent<RockController>().MoveRock())
                        babaBody.MovePosition(babaBody.position + Vector2.left * SpriteLoader.CELL_SIZE);
                }
            }

            if (hit.collider.CompareTag("Word"))
            {
                GameObject word = hit.collider.gameObject;
                if (word.GetComponent<ItemController>().ItemIsMovable(Vector2.left)
                    && word.transform.position.x > -GameManager.WIDTH_RANGE+0.24f)
                {
                    word.GetComponent<Rigidbody2D>().
                        MovePosition(hit.collider.gameObject.GetComponent<Rigidbody2D>().position + Vector2.left * SpriteLoader.CELL_SIZE);
                    babaBody.MovePosition(babaBody.position + Vector2.left * SpriteLoader.CELL_SIZE);
                }
            }

            if (hit.collider.CompareTag("Flag") && WordController.flagIsWin)
            {
                GameManager.isGameOver = true;
            }
        }
        else if (hit.collider == null && transform.position.x > -GameManager.WIDTH_RANGE)
        {
            babaBody.MovePosition(babaBody.position + Vector2.left * SpriteLoader.CELL_SIZE);
        }
    }
    #endregion

    #region collision methods

    #endregion


}
