﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteLoader : MonoBehaviour
{
    #region constants
    public const int TOTAL_SPRITE_COLUMN = 32;
    public const int TOTAL_SPRITE_LINE = 2;
    public const float CELL_SIZE = .24f;
    #endregion

    #region fields
    private static Sprite[] spriteSheet;
    #endregion
    
    #region properties
    public static Sprite[] SpriteSheet { get => spriteSheet; }
    #endregion

    private void Awake()
    {
        spriteSheet = Resources.LoadAll<Sprite>("Art/BabaIsYouSprites");
    }
}
