﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private Text timeElapsed;
    private Text playerMoves;
    private GameObject gameOver;
    private Text stats;

    public Text TimeElpased { get => timeElapsed; set => timeElapsed = value; }
    public Text PlayerMoves { get => playerMoves; set => playerMoves = value; }

    private void Awake()
    {
        timeElapsed = transform.Find("TextArea/Panel/Time").GetComponent<Text>();
        playerMoves = transform.Find("TextArea/Panel/Movements").GetComponent<Text>();
        gameOver = transform.Find("GameOver").gameObject;
        stats = transform.Find("GameOver/Messages/Stats").GetComponent<Text>();
    }
    public void DisplayTime(float timeElapsed)
    {
        float minutes = Mathf.FloorToInt(timeElapsed / 60);
        float seconds = Mathf.FloorToInt(timeElapsed % 60);
        TimeElpased.text = string.Format("{00:00}:{1:00}", minutes, seconds);
    }

    public void GameOver(double score)
    {
        Text timeToComplete = TimeElpased;
        Text movesToComplete = PlayerMoves;
        stats.text = "Questions : " + Mathf.FloorToInt(QuestionManager.successPercentage) + "%\nTime : " + timeToComplete.text + "\n" + movesToComplete.text + "\n\nFinal score \n" + Math.Floor(score);
        gameOver.gameObject.SetActive(true);
    }
}
