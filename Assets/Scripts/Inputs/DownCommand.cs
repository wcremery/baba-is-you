﻿using UnityEngine;

internal class DownCommand : Command
{
    public override void Execute(Playable actor) => actor.MoveDown();

    public override void Undo(Playable actor) => actor.MoveUp();
}