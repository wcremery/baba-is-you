﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHistory
{
    private Command lastCommand;
    private Playable lastPlayable;
    
    public Command LastCommand { get => lastCommand; set => lastCommand = value; }
    public Playable LastPlayable { get => lastPlayable; set => lastPlayable = value; }

    public InputHistory(Command lastCommand, Playable lastPlayable)
    {
        this.lastCommand = lastCommand;
        this.lastPlayable = lastPlayable;
    }
}
