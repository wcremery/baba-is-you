﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    #region fields
    private Command upArrow;
    private Command rightArrow;
    private Command downArrow;
    private Command leftArrow;
    #endregion

    private void Awake()
    {        
        upArrow = new UpCommand();
        rightArrow = new RightCommand();
        downArrow = new DownCommand();
        leftArrow = new LeftCommand();
    }

    public Command HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow)) return upArrow;
        else if (Input.GetKeyDown(KeyCode.RightArrow)) return rightArrow;
        else if (Input.GetKeyDown(KeyCode.DownArrow)) return downArrow;
        else if (Input.GetKeyDown(KeyCode.LeftArrow)) return leftArrow;

        return null;
    }
}
