﻿internal class UpCommand : Command
{
    public override void Execute(Playable actor) => actor.MoveUp();

    public override void Undo(Playable actor) => actor.MoveDown();
}