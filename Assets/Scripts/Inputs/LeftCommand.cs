﻿internal class LeftCommand : Command
{
    public override void Execute(Playable actor) => actor.MoveLeft();

    public override void Undo(Playable actor) => actor.MoveRight();
}