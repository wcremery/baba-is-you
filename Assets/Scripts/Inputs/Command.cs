﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Command
{
    public abstract void Execute(Playable actor);
    public abstract void Undo(Playable actor);
}
