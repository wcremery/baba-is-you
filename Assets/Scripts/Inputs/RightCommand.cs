﻿internal class RightCommand : Command
{
    public override void Execute(Playable actor) => actor.MoveRight();

    public override void Undo(Playable actor) => actor.MoveLeft();
}