﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ItemController : MonoBehaviour
{
    private Rigidbody2D itemBody;
    private SpriteRenderer itemSpriteRenderer;
    private Sprite[] itemAnimationsArray = new Sprite[3];

    private void Awake()
    {
        itemBody = gameObject.GetComponent<Rigidbody2D>();
        itemSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    public void Start()
    {
        FillArrayAnimations();
        StartCoroutine(ItemAnimation());
    }

    public void FillArrayAnimations()
    {
        string name = itemSpriteRenderer.sprite.name;
        int index = -1;
        for (int i = 0; i < SpriteLoader.SpriteSheet.Length; ++i)
        {
            if (name.Equals(SpriteLoader.SpriteSheet[i].name)) index = i;
        }
        itemAnimationsArray[0] = SpriteLoader.SpriteSheet[index];
        itemAnimationsArray[1] = SpriteLoader.SpriteSheet[index + 32];
        itemAnimationsArray[2] = SpriteLoader.SpriteSheet[index + 64];
    }

    public IEnumerator ItemAnimation()
    {
        int index = 0;

        while (index < itemAnimationsArray.Length)
        {
            itemSpriteRenderer.sprite = itemAnimationsArray[index];
            ++index;
            yield return new WaitForSeconds(.8f);
            yield return 0;
        }

        StartCoroutine(ItemAnimation());
    }

    public bool ItemIsMovable(Vector2 direction)
    {
        RaycastHit2D hit = Physics2D.Raycast(itemBody.position, direction, SpriteLoader.CELL_SIZE);
        if (hit.collider == null) return true;
        return false;
    }
}
