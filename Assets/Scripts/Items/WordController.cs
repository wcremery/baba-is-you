﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class WordController : MonoBehaviour
{
    private GameObject[] words;
    private readonly List<GameObject> isList = new List<GameObject>();

    public static bool babaIsYou = false;
    public static bool wallIsStop = false;
    public static bool rockIsStop = false;
    public static bool flagIsWin = false;
    public static bool rockIsPush = false;
    public static bool rockIsWin = false;
    public static bool babaIsWall = false;
    public static bool babaIsWin = false;

    private void Awake()
    {
        InitializeWords();
    }

    private void InitializeWords()
    {
        words = GameObject.FindGameObjectsWithTag("Word");

        for (int i = 0; i < words.Length; ++i)
        {
            words[i].AddComponent<ItemController>();
            if (words[i].name.Contains("Is")) isList.Add(words[i]);
        }
    }

    private void Update()
    {
        if (babaIsYou && babaIsWin) GameManager.isGameOver = true;

        babaIsYou = false;
        wallIsStop = false;
        rockIsStop = false;
        flagIsWin = false;
        rockIsPush = false;
        rockIsWin = false;
        babaIsWall = false;
        babaIsWin = false;

        foreach (GameObject isWord in isList)
        {            
            RaycastHit2D hitWordUp = Physics2D.Raycast(isWord.transform.position, Vector2.up, SpriteLoader.CELL_SIZE);
            RaycastHit2D hitWordDown = Physics2D.Raycast(isWord.transform.position, Vector2.down, SpriteLoader.CELL_SIZE);
            RaycastHit2D hitWordRight = Physics2D.Raycast(isWord.transform.position, Vector2.right, SpriteLoader.CELL_SIZE);
            RaycastHit2D hitWordLeft = Physics2D.Raycast(isWord.transform.position, Vector2.left, SpriteLoader.CELL_SIZE);

            if (hitWordUp.collider != null && hitWordDown.collider != null)
            {
                string word1 = hitWordUp.collider.gameObject.name;
                string word2 = hitWordDown.collider.gameObject.name;
                CheckForTheExpression(word1 + word2);
            }
            if (hitWordLeft.collider != null && hitWordRight.collider != null)
            {
                string word1 = hitWordLeft.collider.gameObject.name;
                string word2 = hitWordRight.collider.gameObject.name;
                CheckForTheExpression(word1 + word2);
            }
        }
    }

    private void CheckForTheExpression(string expression)
    {
        switch (expression)
        {
            case "BabaYou":
                babaIsYou = true;
                break;
            case "FlagWin":
                flagIsWin = true;
                break;
            case "RockPush":
                rockIsPush = true;
                break;
            case "RockWin":
                rockIsWin = true;
                break;
            case "BabaWall":
                babaIsWall = true;
                break;
            case "BabaWin":
                babaIsWin = true;
                break;
            default:
                break;
        }
    }
}
