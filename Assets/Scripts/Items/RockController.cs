﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class RockController : ItemController
{
    private bool rockIsMovable = false;

    public bool MoveRock()
    {
        Vector2 lastDirection = BabaController.lastDirection;
        rockIsMovable = ItemIsMovable(lastDirection);

        if (WordController.rockIsPush && rockIsMovable)
        {
            Rigidbody2D rockBody = gameObject.GetComponent<Rigidbody2D>();
            rockBody.MovePosition(rockBody.position + lastDirection * SpriteLoader.CELL_SIZE);
            return true;
        }
        return false;
    }
}
