﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : MonoBehaviour
{
    private GameObject[] wallsArray;

    private void Awake()
    {
        InitializeWords();
    }

    private void InitializeWords()
    {
        wallsArray = GameObject.FindGameObjectsWithTag("Wall");

        for (int i = 0; i < wallsArray.Length; ++i)
        {
            wallsArray[i].AddComponent<ItemController>();
        }
    }
}
